#
#  Be sure to run `pod spec lint TestPrivatePod.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "TestPrivatePod"
  s.version      = "0.0.1"
  s.summary      = "TestPrivatePod. good"

  s.description  = <<-DESC
TestPrivatePod. good TestPrivatePod. good TestPrivatePod. good
                   DESC

  s.homepage     = "https://lihongtao@bitbucket.org/lihongtao/testprivatepod"

  s.license      = { :type => "MIT", :file => "FILE_LICENSE" }

  s.author             = { "lihongtao" => "1004646858@qq.com" }

  s.platform     = :ios, "8.0"


  s.source       = { :git => "https://lihongtao@bitbucket.org/lihongtao/testprivatepod.git", :tag => "#{s.version}" }


  s.source_files  = "Classes", "Classes/**/*.{h,m}"
  s.exclude_files = "Classes/Exclude"

  s.public_header_files = "Classes/**/*.h"

  s.requires_arc = true

end
